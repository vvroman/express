var os = require('os');
var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var session = require('express-session');

//mongoose
var Mongostore = require('connect-mongo')(session);

var mongoose = require('mongoose');
mongoose.Promise = global.Promise;
mongoose.connect( 'mongodb://localhost/express5');

var db = mongoose.connection;

db.on( 'error', function ( err) {
  console.error( 'connection error:', err.message);
});

db.once( 'open', function () {
  console.info( 'Connected to DB!');
});


var User =  require('./models/user');

//passportjs
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;




//route handlers
var index = require('./routes/index');
var maps = require('./routes/maps');
var chat = require('./routes/chat');
var profile = require('./routes/profile');
var signin = require('./routes/signin');
var editor = require('./routes/editor');

var app = express();
const nconf = require('nconf');
nconf.argv()
  .env()
  .file({ file: 'appconf.json' });


// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser('secret'));
var session = app.use(session({
  secret: 'secret',
  resave: false,
  saveUninitialized: true,
  cookie: { secure: false, httpOnly: false },
  store: new Mongostore({ url: 'mongodb://localhost/express5' })
}))
app.use(passport.initialize());
app.use(passport.session());





app.use(require('node-sass-middleware')({
  src: path.join(__dirname, 'public'),
  dest: path.join(__dirname, 'public'),
  indentedSyntax: false,
  sourceMap: true
}));

//Passport setup
passport.serializeUser(User.serializeUser());
passport.deserializeUser(User.deserializeUser());
passport.use(new LocalStrategy(
  {
    usernameField: 'username',
    passwordField: 'password'
  }, User.authenticate()
));

// check user middleware
var checkAuth = function (req,res,next) {
  if (req.user) 
    {next()} 
  else
    {res.redirect('/')}
}

//routes
app.use(express.static(path.join(__dirname, 'public')));
app.use('/', index);
app.use('/maps',checkAuth, maps);
app.use('/chat', checkAuth, chat);
app.use('/profile', checkAuth, profile);
app.use('/editor', checkAuth, editor);

app.use('/signin',
  passport.authenticate('local', {
 //  successRedirect: '/',
    failureRedirect: '/qwerty',
    failureFlash: false
  }),
  signin);


app.use('/register', (req, res, next)=>{
var prf=prf || {aaa:'bbb',ccc:'ddd'};
console.log(`\n\n register: ${req.body.username} ${req.body.password} \n\n`)
User.register(new User({username: req.body.username}),req.body.password, (err)=>{
  if (err) {
    console.log('registration fail:',err)
    return next(err);
  
}
res.redirect('/');
});
})

app.use('/signout', (req, res, next)=>{

console.log(`\n\n logOut:  \n\n`)
  req.logout();
  res.json({status:'success'});;
  res.end();

  //res.redirect('/');
})

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error', { menu: [] });
});

module.exports = app; //{app:app, server:server};
