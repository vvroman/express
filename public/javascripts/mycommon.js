'use strict'

var signinBtn = document.getElementById('signinbtn');
var signoutBtn = document.getElementById('signoutbtn');

//var formData = new FormData();
//formData.append ('username',document.getElementById('username').value)
//formData.append ('password',document.getElementById('password').value)

if (signinBtn) {
    signinBtn.addEventListener('click', (ev) => {
        console.log('trying to sign-in');
        var options = {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
 //                 'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8'
            },
            credentials: 'include',
            
            //body: formData
               body: JSON.stringify( {
                    username: document.getElementById('username').value,
                    password: document.getElementById('password').value
               }) 
            // body: JSON.stringify(
            //     {
            //         action: 'signin',
            //         

            //     })
        }
        fetch('/signin', options)
            .then((res) => { return res.json() })
            .then((json) => { if (json.status = 'success') window.location.reload(true) });
    })
}


if (signoutBtn) {
    signoutBtn.addEventListener('click', (ev) => {
        console.log('trying to sign-out');
        var options = {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            credentials: 'include',
            body: JSON.stringify(
                {
                    action: 'signout'
                })
        }
        fetch('/signout', options)
            .then((res) => { return res.json() })
            .then((json) => { if (json.status = 'success') window.location.reload(true)  });
    })
}

console.log(document.cookie);