//send by button
document.getElementById('sendMsg').onclick=sendMessage;
// or by "ctrl-enter"
document.getElementById('messagetext').onkeydown=(e)=>{
    if (e.ctrlKey && e.keyCode==13) {
        sendMessage();
    }
}

//connect to server
var socket = io();
    socket.on('connect', ()=>{console.log('connect!',socket.id)});
    socket.on('msgToAll', (msg) =>  {showIncommingMessage (msg)} );
    socket.on('disconnect', (e)=>{console.log('disconnect',e)})


function sendMessage(){
    let msg = document.getElementById('messagetext').value;
    let name = document.getElementById('name').value;
    showOutgoingMessage(msg);
    socket.emit('msgToServer',{msg:msg,to:'all',from:name});
}

function showIncommingMessage(msg){
    let container=document.getElementById('chat-messages');
    let html=`<div class="row">`
        +`<div col col-md-12><h5>Message from: ${msg.from}</h5><\div>`
        +`<div class="well col col-md-11 msgin"><p>${msg.msg}</p></div></div>`;
    container.insertAdjacentHTML('beforeend',html);
    window.scrollTo(0,document.body.scrollHeight);
}

function showOutgoingMessage(msg){
    let container=document.getElementById('chat-messages');
    let html=`<div class="row"><div class="well col col-md-11 col-md-offset-1 msgout"><p>`
        + msg
        + `</p></div></div>`;
    container.insertAdjacentHTML('beforeend',html);
    window.scrollTo(0,document.body.scrollHeight);
}