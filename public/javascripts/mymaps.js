'use strict'

var io = io('');

//setup buttons callbacks
document.getElementById('findbyhtml').onclick = findByHTML;
document.getElementById('findbygoogle').onclick = findByGoogle;

//find using fake wifi information.
document.getElementById('findbygooglewithhint').onclick = findByGoogle;

//let it be global placeholder for map objects...
var map = {};

//callback for google script
function initMap() {
	map.map = new google.maps.Map(document.getElementById('map'), {
		//hardcoded KIEW
		center: { lng: 30.30, lat: 50.30 },
		scrollwheel: true,
		zoom: 6
	});
}

function findByHTML() {
	navigator.geolocation.getCurrentPosition(
		(crd) => {
			let position = {
				lat: crd.coords.latitude,
				lng: crd.coords.longitude,
				acc: crd.coords.accuracy
			}
			showLocOnMap(position);
		}),
		(err) => { console.log(err) }

}

function findByGoogle(ev) {

	const apiKey = 'AIzaSyCReLyFfsVhnfFolY-zGapGOw9azPZRMG0';

	const requestOptions = {
		method: 'POST',
		headers: {
			'Accept': 'application/json',
			'Content-Type': 'application/json'
		}
	}
	// "Hint for google"
	var mapsRequestBody = {
		wifiAccessPoints: [
			{
				"macAddress": "54:a0:50:75:9e:54",
				"channel": 36,
				"signalStrength": -43
			},
			{
				"macAddress": "D8:5D:4C:F3:A4:C6",
				"channel": 1,
				"signalStrength": -60
			}
		]
	}

	if (ev.target.dataset.usehint == '1') {
		requestOptions.body = JSON.stringify(mapsRequestBody);
	}

	const url = 'https://www.googleapis.com/geolocation/v1/geolocate?key=' + apiKey

	fetch(url, requestOptions)
		.then(function (res) { return res.json() })
		.then((json) => {

			let position = {
				lat: json.location.lat,
				lng: json.location.lng,
				acc: json.accuracy
			}
			showLocOnMap(position);
		});

}

function showLocOnMap(position) {

	map.map.setCenter(position);
	map.map.setZoom(15);

	map.infoWindow = map.infoWindow || new google.maps.InfoWindow({ map: map.map });

	map.infoWindow.setPosition(position);
	map.infoWindow.setContent(`You are here!. <br>lng: ${position.lng}<br>lat: ${position.lat}` +
		`<br> accuracy: ${position.acc}m`);
	map.infoWindow.open(map.map);
	io.emit('newlocation', position);
}

