
var io = io('');

tinymce.init({
    height: 300,
    selector: '#mce',
    plugins: 'save',
    toolbar: 'save | undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent',
    //toolbar: 'save bold italic',
    statusbar: false,
    menubar:false,
    save_enablewhendirty: true,
    save_onsavecallback: (ev) => {
        
        //data-docid is defined when article loaded
        if (document.getElementById('doctitle').dataset.docid) 
        {
            console.log('Saved', ev);
        var options = {
            method: 'PUT',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            credentials: 'include',
            body: JSON.stringify({
                id: document.getElementById('doctitle').dataset.docid,
                body: tinymce.activeEditor.getContent(),
                title: document.getElementById('doctitle').value
            })

        }
        console.log('fetching...', options)
        fetch(`/editor/api/${document.getElementById('doctitle').dataset.docid}`, options)
            .then((res) => { return res.json() })
            //if ok - update article list
            .then((json) => { 
                var titleDiv=document.querySelector(`div[data-id="${json._id}"]`);
                console.log('selector=',`[data-id="${json._id}"]`);
                    titleDiv.innerHTML=json.title;
                console.log(json) })
            .catch((err) => { console.log(err) });
        }

    }
});


//load article list on page load
document.addEventListener('DOMContentLoaded', function() {
   
reloadArticleList();

});

document.getElementById('newarticlebtn').addEventListener('click', (ev) => {

    fetch('/editor/api', {
        method: 'POST',
        headers: {
            'Accept' : 'application/json',
            'Content-Type' : 'application/json'
        },
        credentials: 'include'
    })
        .then((res) => { return res.json() })
        .then((json) => {
            tinymce.activeEditor.setContent(json.body, { format: 'raw' });
            document.getElementById('doctitle').value = json.title;
            console.log(json)
            //save docID in header dataset for update.
            document.getElementById('doctitle').dataset.docid = json._id;
            reloadArticleList();
        })
        .catch((err) => { console.log(err) })
});


document.getElementById('wrarticlelist').addEventListener('click', (ev) => {
    
    if ( ev.target.dataset.id ) {

        fetch(`/editor/api/${ev.target.dataset.id}`, {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            credentials: 'include'
        })
            .then((res) => { return res.json() })
            .then((json) => {
                tinymce.activeEditor.setContent(json.body, { format: 'raw' });
                document.getElementById('doctitle').value = json.title;
                //save docID in header dataset for update.
                document.getElementById('doctitle').dataset.docid = ev.target.dataset.id;
                console.log(json)
            })
            .catch((err) => { console.log(err) })

    }

})

function reloadArticleList() {

fetch('/editor/api/', {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            credentials: 'include',
            method: 'GET'
        })
            .then((res) => { return res.json() })
            .then((json) => {
                console.log(json)
                var wrapper=document.getElementById('articlelist');
                wrapper.innerHTML='';
                json.forEach( (article)=>{
                    //div(data-id=article._id).list-group-item.text-info=article.titl
                    wrapper.insertAdjacentHTML('beforeEnd',
                    `<div data-id="${article._id}" class="list-group-item text-info">${article.title} <span class="bg-danger pull-right glyphicon glyphicon-trash"> </span></div>`)
                })
            })
            .catch((err) => { console.log(err) })
    


}