var mongoose = require('mongoose');
var prf = require('./profile');

var Schema = mongoose.Schema;
var passportLocalMongoose = require('passport-local-mongoose');


var UserSchema = {

    // _id will be created by Mongo

 

    role: {
        stuff: [{
            type: Schema.Types.ObjectId,
            ref: 'Rest'
        }],
        restorator: [{
            type: Schema.Types.ObjectId,
            ref: 'Rest'
        }]
    },

    auth: {     

        local:{ 
            username: String
        }

        // fb: Schema.Types.Mixed,
        // gl: Schema.Types.Mixed
    },

    birthday: {
        type: Date
    },

    gender: {
        type: String
    },


    createTime : {
        type: Date, default: Date.now
    },
    modifyTime : {
        type: Date, default: Date.now
    },

}

var User = new Schema(UserSchema);

User.virtual('username')
    .get( () => {return this.auth.local.username })
    .set( (usr) => {this.auth.local.username = usr });

User.virtual('hash')
    .get( () => {return this.auth.local.hash })
    .set( (hash) => {this.auth.local.hash = hash });

User.virtual('salt')
    .get( () => {return this.auth.local.salt })
    .set( (salt) => {this.auth.local.salt = salt });

User.virtual('sex')
    .get( () => {return this.auth.local.username + this.auth.local.username })
//    .set( (s) => {this.auth.local.username = this.auth.local.username+"!SEX" });


User.plugin(passportLocalMongoose, {
//    usernameField: 'auth.local.username',
//    hashField:'auth.local.hash',
//    saltField:'auth.local.salt',
    
//    limitAttempts: false,

    
    
     });

module.exports = mongoose.model('User', User);
