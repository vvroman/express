var mongoose = require( 'mongoose');
var Schema  = mongoose.Schema;

const schemaFields = {

id: {
       type: Schema.Types.ObjectId, ref: 'User',
       
    },
provider: {
        type: String,
        required: false
    },
profile: {
        userid: {type: String},
        username: {type: String},
        displayName: {type: String},
        name: {
            familyName: {type: String},
            givenName: {type: String},
            middleName: {type: String} 
        },
        gender: {type: String},
        profileUrl: {type: String},
        emails: [{type: String}]
}   

}

var schema = new Schema({});
schema.add(schemaFields);


exports.model =  mongoose.model('Profile',schema);  
exports.schema = schema;
