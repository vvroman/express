var mongoose = require( 'mongoose');
var Schema  = mongoose.Schema;

const schemaFields = {
    type : {
        type: String,
        required: true
    },
    provider : {
        type: String,
        required: true
    },
    appID : {
        type: String,
        required: true
    },
    appSecret : {
        type: String,
        required: true
    },
    appCallback : {
        type: String,
        required: true
    },
    appAuthPath : {
        type: String,
        required: true
    }
}

Schema.add(schemaFields);

var _schema = new Schema();
module.exports = mongoose.model('oAuthCloent',_schema);