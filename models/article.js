var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var ArticleSchema = {

    author: {
        type: Schema.Types.ObjectId, 
        ref: 'User' 
    },
    title : {
        type: String, default: 'Unnamed article'
    },

    createTime : {
        type: Date, default: Date.now
    },
// Sorry guys... it should be modi_f_yTime... but it is internal field of MODEL.
// TODO: FIX after debug.
    modityTime : {
        type: Date, default: Date.now
    },
    body: {
        type: String
    }
}

var Article= new Schema(ArticleSchema);

//update modify timestamp
// Article.post('findOneAndUpdate', (doc,next)=>{ 
//     doc.modityTime = new Date();
//     doc.save();
//     next()})

//from mongo DOC http://mongoosejs.com/docs/middleware.html
Article.pre('findOneAndUpdate', function() {
  this.update({},{ $set: { modityTime: new Date() } });
});

module.exports = mongoose.model('Article', Article);