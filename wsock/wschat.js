var os = require('os');
var wss = require('./index');

wss.on('connection', function (client) {

	var ipList = [];

	//prepare IP list	
	for (var interface in os.networkInterfaces()) {
		if (!os.networkInterfaces()[interface][0].internal) {
			ipList.push(os.networkInterfaces()[interface][0].address);
		}
	}

	//count clients
	//send welcome message
	wss.clients( (err,clients) =>{
		const welcomeMsg = {
		from: 'Server',
		to: 'all',
		msg: `Welcome to the chat. Your websocket Id: ${client.id},`
		+` our IP address list: ${ipList},`
		+` we have ${clients.length} connected users`}	
		client.emit('msgToAll', welcomeMsg );
	})

	client.on('msgToServer', (msg) => {
		//route messages
		switch (msg.to) {
			case 'all': 
				client.broadcast.emit('msgToAll', msg)
				break;
		}
	});
});