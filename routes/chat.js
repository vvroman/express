var express = require('express');
var router = express.Router();

const common = require('./common')

/* GET users listing. */
router.all('/', function (req, res, next) {
    res.render('chat',
        {
            isAuth: !!req.user,
            title: req.session.username,
            menu: common.menu,
            hlMenuId: "chat"
        });
    // res.end('Hello menu3!')
    // res.io.emit("socketToMe", "Message");  
});

module.exports = router;