var express = require('express');
var router = express.Router();
//var user = require('../models/user');
const common = require('./common');




/* GET home page. */
router.get('/', function (req, res, next) {
  console.log('user: ' + JSON.stringify(req.user));

  
  if (req.user) {
    console.log(JSON.stringify(req.user));
    //    user.findById(req.user)
  }

  res.render('index',
    {
      isAuth: !!req.user,
      title: !!req.user ? req.user.username : '',
      menu: common.menu,
      hlMenuId: "index"
    });
});

module.exports = router;
