var express = require('express');
var router = express.Router();

const common = require('./common')
/* GET users listing. */
router.all('/', function (req, res, next) {
    res.render('profile',
        {
            isAuth: !!req.user,
            title: req.session.username,
            menu: common.menu,
            hlMenuId: "profile"
        });
});

module.exports = router;