const common = require('./common');
var article = require('../models/article');
var express = require('express');
var router = express.Router();


router.get('/', function (req, res) {

    var query = article.find({ author: req.user._id }).then((articles) => {

        res.render('editor', {
            isAuth: !!req.user,
            title: req.user ? req.user.username : '',
            menu: common.menu,
            hlMenuId: "editor",
            articles: articles
        })
    })
        .catch((err) => { console.log(err) });;
});


//REST API
router.get('/api/', function (req, res) {

    var query = article.find({ author: req.user._id }).sort({modityTime: 1}).then((articles) => {

        res.json(articles); 
        
    })
        .catch((err) => { console.log(err) });;
});

router.get('/api/:id',
    function (req, res) {
        article.findOne({ _id: req.params.id, author: req.user._id }).then((article) => {
            console.log(JSON.stringify(article))
            res.json(article);
            res.end();
        });
    });

router.put('/api/:id',
    function (req, res) {
        console.log('put...',req.body);
        var query = article.findOneAndUpdate(
            {
                _id: req.params.id,
                author: req.user._id
            },
            {
                title: req.body.title,
                body: req.body.body
            },
            {
                new: true,
                returnNewDocument: true // it seems it doesn't work :(
            })
            .then((article) => {
                console.log('\nUpdated article:', JSON.stringify(article))
                res.json(article);
            })
            .catch((err) => { console.log(err) });

    });


router.post('/api/',
    function (req, res) {
        var doc = new article({
            title: 'Unnamed Article',
            body: 'Place some text here',
            author: req.user._id
        });
        doc.save((err) => { console.log(err) })
        res.json(doc);

        //just for shure
        res.end();
    });

module.exports = router;


 // console.log(mce);