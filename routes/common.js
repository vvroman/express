//menu

module.exports.menu = [
  {
    id: "index",
    text: "Index",
    link: "/"
  },
  {
    id: "maps",
    text: "Maps",
    link: "/maps"
  },
  {
    id: "chat",
    text: "Chat",
    link: "/chat"
  },
  {
    id: "profile",
    text: "Profile",
    link: "/profile"
  },

  {
    id: "editor",
    text: "Editor",
    link: "/editor"
  }
]